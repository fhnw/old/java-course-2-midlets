package examples;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;


public class Etch_screen extends Canvas {
	private int Screen_width;
	private int Screen_height;
	private int Cursor_x;
	private int Cursor_y;

	public Etch_screen(Etch_a_sketch midlet) {
		super();
		Screen_width = getWidth();
		Screen_height = getHeight();
		Cursor_x = Screen_width / 2;
		Cursor_y = Screen_height / 2;
	}

	protected void paint(Graphics g) {
		g.setColor(0, 0, 0);
		g.fillRect(Cursor_x, Cursor_y, 2, 2);
	}

	protected void keyPressed(int keyCode) {
		if (getGameAction(keyCode) == UP) {
			Cursor_y -= 1;
		} else if (getGameAction(keyCode) == LEFT) {
			Cursor_x -= 1;
		} else if (getGameAction(keyCode) == RIGHT) {
			Cursor_x += 1;
		} else if (getGameAction(keyCode) == DOWN) {
			Cursor_y += 1;
		}
		repaint();
	}
}
