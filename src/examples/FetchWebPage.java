package examples;

import java.io.*;
import javax.microedition.midlet.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;

public class FetchWebPage extends MIDlet implements Runnable, CommandListener {
	private Form myForm;
	private TextField urlPrompt;
	private StringItem urlText;
	private Display display;

	public FetchWebPage() {
		super();
		display = Display.getDisplay(this);
		myForm = new Form("Simple Browser");
		urlPrompt = new TextField("Enter URL:", "http://www.ibm.com/", 50, TextField.URL);
		urlText = new StringItem(null, "");
		myForm.append(urlPrompt);
		myForm.append(urlText);
		myForm.addCommand(new Command("Exit", Command.EXIT, 0));
		myForm.addCommand(new Command("Load", Command.ITEM, 1));
		myForm.setCommandListener(this);
	}

	public void startApp() {
		display.setCurrent(myForm);
	}

	public void pauseApp() {
	}

	public void destroyApp(boolean unconditional) {
	}

	public void commandAction(Command cmd, Displayable disp) {
		Thread urlThread;
		if (cmd.getLabel() == "Load") {
			urlThread = new Thread(this);
			urlThread.start();
			display.setCurrent(myForm);
		} else {
			notifyDestroyed();
		}
	}

	public void run() {
		String result;
		StringBuffer buf = new StringBuffer();
		InputStream strIn = null;
		HttpConnection con = null;
		try {
			con = (HttpConnection) Connector.open(urlPrompt.getString());
			strIn = con.openInputStream();

			// Read till the connection is closed.
			int ch = strIn.read();
			while (ch != -1) {
				buf.append((char) ch);
				ch = strIn.read();
			}
			result = buf.toString();
		} catch (Exception e) {
			result = e.toString();
		} finally {
			try {
				strIn.close();
			} catch (Exception e) {
			}
			try {
				con.close();
			} catch (Exception e) {
			}
		}
		urlText.setText(result);
	}
}
