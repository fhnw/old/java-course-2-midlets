package examples;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class GuiTests extends MIDlet implements CommandListener {
	// display manager
	Display display;

	// six possible screens
	List menu;      // the main screen
	TextBox input;  // 1. test of a textbox
	List choose;    // 2. test of a list
	final Alert soundAlert = new Alert("sound Alert"); // 3. alert
	Form today = new Form("Today's date"); // 4. form with date
	Form form = new Form("Form for Stuff"); // 5. form with controls

	// Ticker re-used for all screens
	Ticker ticker = new Ticker("Test GUI Components");
	
	// GUI elements
	DateField date = new DateField("Today's date: ", DateField.DATE);
	Gauge gauge = new Gauge("Progress Bar", false, 20, 9);
	TextField textfield = new TextField("TextField Label", "abc", 50, TextField.ANY);

	// commands
	static final Command backCommand = new Command("Back", Command.BACK, 0);
	static final Command mainMenuCommand = new Command("Main", Command.SCREEN, 1);
	static final Command exitCommand = new Command("Exit", Command.STOP, 2);

	// constructor - we could assemble static items here...
	public GuiTests() {
	}

	// Instead, we de-allocate all GUI-Objects when paused,
	// and re-allocate them when we start
	public void startApp() throws MIDletStateChangeException {
		display = Display.getDisplay(this);
		menu = new List("Test Components", Choice.IMPLICIT);
		menu.append("Test TextBox", null);
		menu.append("Test List", null);
		menu.append("Test Alert", null);
		menu.append("Test Date", null);
		menu.append("Test Form", null);
		menu.addCommand(exitCommand);
		menu.setCommandListener(this);
		menu.setTicker(ticker);

		// show the main menu
		mainMenu();
	}

	// Save resources by de-allocating GUI elements
	public void pauseApp() {
		display = null;
		choose = null;
		menu = null;
		ticker = null;
		form = null;
		today = null;
		input = null;
		gauge = null;
		textfield = null;
	}

	public void destroyApp(boolean unconditional) {
	}

	// main menu
	void mainMenu() {
		display.setCurrent(menu);
	}

	// 1. TextBox
	public void testTextBox() {
		input = new TextBox("Enter Some Text:", "", 10, TextField.ANY);
		input.setTicker(new Ticker("Testing TextBox"));
		input.addCommand(backCommand);
		input.setCommandListener(this);
		input.setString("");
		display.setCurrent(input);
	}

	// 2. List
	public void testList() {
		choose = new List("Choose Items", Choice.MULTIPLE);
		choose.setTicker(new Ticker("Testing List"));
		choose.addCommand(backCommand);
		choose.setCommandListener(this);
		choose.append("Item 1", null);
		choose.append("Item 2", null);
		choose.append("Item 3", null);
		display.setCurrent(choose);
	}

	// 3. Alert
	public void testAlert() {
		soundAlert.setTicker(new Ticker("Testing alert"));
		soundAlert.setType(AlertType.ERROR);
		soundAlert.setTimeout(5000); // wait 5 seconds
		soundAlert.setString("** ERROR **");
		display.setCurrent(soundAlert);
	}

	// 4. Form with date
	public void testDate() {
		if (today.size() == 0) {
			today.setTicker(new Ticker("Testing form with date"));
			java.util.Date now = new java.util.Date();
			date.setDate(now);
			today.append(date);
			today.addCommand(backCommand);
			today.setCommandListener(this);
		}
		display.setCurrent(today);
	}

	// 5. Form with controls
	public void testForm() {
		if (form.size() == 0 ) {
			form.setTicker(new Ticker("Testing form with controls"));
			form.append(gauge);
			form.append(textfield);
			form.addCommand(backCommand);
			form.setCommandListener(this);
		}
		display.setCurrent(form);
	}

	// Commands: exit, back, menu-selection
	public void commandAction(Command c, Displayable d) {
		String label = c.getLabel();
		if (label.equals("Exit")) {
			notifyDestroyed();
		} else if (label.equals("Back")) {
			mainMenu();
		} else { // list selection
			List down = (List) display.getCurrent();
			switch (down.getSelectedIndex()) {
			case 0:
				testTextBox();
				break;
			case 1:
				testList();
				break;
			case 2:
				testAlert();
				break;
			case 3:
				testDate();
				break;
			case 4:
				testForm();
				break;
			}
		}
	}
}