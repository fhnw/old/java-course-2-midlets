package examples;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

public class HelloWorld extends MIDlet implements CommandListener {
	private Form myForm;
	
	public HelloWorld() {
		super();
		myForm = new Form("Hello World!");
		myForm.append( new StringItem(null, "Hello, world!"));
		myForm.addCommand(new Command("Exit", Command.EXIT, 0));
		myForm.setCommandListener(this);
	}

	protected void startApp() throws MIDletStateChangeException {
		Display.getDisplay(this).setCurrent(myForm);
	}

	protected void pauseApp() {
	}

	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
	}

	public void commandAction(Command arg0, Displayable arg1) {
		notifyDestroyed();
	}

}
