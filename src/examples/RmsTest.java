package examples;

import java.io.*;
import javax.microedition.midlet.*;
import javax.microedition.rms.*;
import javax.microedition.lcdui.*;

public class RmsTest extends MIDlet implements CommandListener {
	private Form myForm;
	private TextField noteText;
	private Display display;
	private RecordStore notes;
	private int recordID;

	public RmsTest() {
		super();
		display = Display.getDisplay(this);
		myForm = new Form("Notepad");
	}

	public void startApp() {
		// if the form is not initialized
		// then create it, and read the old note
		if (myForm.size() == 0) {
			noteText = new TextField(null, "", 256, TextField.ANY);
			myForm.append(noteText);
			myForm.addCommand(new Command("Exit", Command.EXIT, 0));
			myForm.setCommandListener(this);

			try {
				notes = RecordStore.openRecordStore("myNotes", true);
				if (notes.getNumRecords() == 0) {
					noteText.setString("--");
					recordID = -1;
				} else {
					// There is only one record, but we do not know its ID
					// Using an Enumeration, ask for the ID of the first record
					RecordEnumeration IDs = notes.enumerateRecords(null, null, false);
					recordID = IDs.nextRecordId();
					ByteArrayInputStream inStream = new ByteArrayInputStream(notes.getRecord(recordID));
					DataInputStream dis = new DataInputStream(inStream);
					noteText.setString(dis.readUTF());
				}
			} catch (Exception e) {
				noteText.setString("Unable to start!");
			}
		}
		display.setCurrent(myForm);
	}

	public void pauseApp() {
	}

	public void destroyApp(boolean unconditional) {
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			DataOutputStream dataOut = new DataOutputStream(outStream);
			dataOut.writeUTF(noteText.getString());
			byte[] b = outStream.toByteArray();
			
			if (recordID >= 0) {
				notes.setRecord(recordID, b, 0, b.length);
			} else {
				notes.addRecord(b, 0, b.length);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void commandAction(Command cmd, Displayable disp) {
		destroyApp(true);
		notifyDestroyed();
	}

}
