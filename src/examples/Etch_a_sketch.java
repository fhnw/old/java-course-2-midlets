package examples;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class Etch_a_sketch extends MIDlet implements CommandListener {
	private Etch_screen myCanvas;
	private Command myExitCommand = new Command("Exit", Command.EXIT, 1);

	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
	}

	protected void pauseApp() {
	}

	protected void startApp() throws MIDletStateChangeException {
		if (myCanvas == null) {
			myCanvas = new Etch_screen(this);
			myCanvas.addCommand(myExitCommand);
			myCanvas.setCommandListener(this);
		}
		Display.getDisplay(this).setCurrent(myCanvas);
	}

	public void commandAction(Command c, Displayable s) {
		if (c == myExitCommand) {
			notifyDestroyed();
		}
	}

}
